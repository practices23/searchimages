import { Formik, Form, Field } from "formik";
import { useEffect, useState } from "react";
import { Box, Button, Container, Grid } from "@mui/material";
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined';
import FavoriteOutlinedIcon from '@mui/icons-material/FavoriteOutlined';



const App = () => {
  
  const [photos, setPhotos] = useState([])

  
  

  return (
    <Container maxWidth="xl"> 
      <Grid container spacing={2}>
        <Grid item xs={10}>
          <Formik 
            initialValues={{search:''}}
            onSubmit = {async values => {
              const response = await fetch(`https://api.giphy.com/v1/gifs/search?api_key=9HBZF52PGxiHLU0jC7MyYTrEX7DNfDdI&q= ${values.search} &limit=25&offset=0&rating=g&lang=en`);
              const data = await response.json()
              setPhotos(data.data)
              
            }}
          >
            <Form>
              <Field name= "search" />
              <Button type='submit'> Buscar </Button>
            </Form>
          </Formik>
        </Grid>
        <Grid item xs={10}>
          <Box sx={{ display: 'flex', flexWrap: 'wrap'}}>
            <PhotosShow photos={photos} />
          </Box>
        </Grid>
      </Grid>
    </Container>
  )
}

const PhotosShow = ({photos}) => {

  let likes = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];

  const handleLike = (like) => {
    console.log(like)
    likes[like] = (!likes[like])
    console.log(likes)
  }

  useEffect(() => {
    photos=photos;
  }, [likes])
  
  return(
    photos.map ((photo, index) =>
      <Box >
        <img style={{height: '150px',width: '200px'}} src={photo.images.original.url}/>
        <Button key={index} style={styleButton} onClick={() => handleLike(index)}> {likes[index]===false ? <FavoriteBorderOutlinedIcon/> : <FavoriteOutlinedIcon/>} </Button>
      </Box>
    )
  )
}

export default App;

const styleButton ={
  marginLeft: "-3rem",
  marginTop: "-16rem",
  color:'red'
}

